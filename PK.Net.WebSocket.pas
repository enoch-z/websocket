﻿(*
 * WebSocket Class
 *
 * PLATFORMS
 *   Windows / macOS / Linux (Maybe iOS, Android)
 *
 * ENVIRONMENT
 *   Delphi 10.3.x Rio
 *
 * REQUIRED LIBRARY
 *   OpenSSL Files
 *     Windows: libeay32.dll, ssleay32.dll
 *     macOS:   libcrypto.dylib, libssl.dylib
 *     iOS:     libcrypto.a, libssl.a
 *     Android: libcrypto.so, libssl.so
 *     Linux:   libcrypto.so, libssl.so
 *
 *   See also below URL
 *     http://docwiki.embarcadero.com/RADStudio/Rio/en/Securing_Indy_Network_Connections
 *     http://docwiki.embarcadero.com/RADStudio/Rio/en/OpenSSL
 *     https://indy.fulgan.com/SSL/
 *
 * LICENSE
 *   Copyright (C) 2019 HOSOKAWA Jun (Twitter: @pik)
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2019/04/04 Version 1.0.0
 *   2019/04/11 Version 1.0.1  Change CloseEvent
 *   2019/04/12 Version 1.0.2  Fixed Closing Bug
 *   2019/04/15 Version 1.0.3  Fixed Reconnect Bug
 *   2019/04/17 Version 1.0.4  Fixed Log Bug
 *   2019/04/19 Version 1.0.5  Added ResetHeartbeat
 *   2019/04/21 Version 1.0.6  Added BytesToString, BytesToHex
 *   2019/04/24 Version 1.0.7  Fixed BytesToHex Hints
 *   2019/06/06 Version 1.0.8  Fixed Change SocketClosed Methods
 *   2020/03/19 version 1.0.9  Fixed Not use SSL Connection
 *   2020/03/27 version 1.0.10 Add AsyncEvent Flag
 *
 * USAGE
 *  // Create
 *  procedure TfrmMain.FormCreate(Sender: TObject);
 *  begin
 *    FWebSocket := TWebSocket.Create(Self);
 *    FWebSocket.OnEstablished := WebSocketEstablished;
 *    FWebSocket.OnError := WebSocketError;
 *    FWebSocket.OnText := WebSocketText;
 *  end;
 *
 *  // Destroy
 *  procedure TfrmMain.FormDestroy(Sender: TObject);
 *  begin
 *    FWebSocket.DisposeOf;
 *  end;
 *
 *  // Connect
 *  procedure TfrmMain.Button1Click(Sender: TObject);
 *  begin
 *    FWebSocket.Connect('wss://echo.websocket.org');
 *  end;
 *
 *  // Send Text
 *  procedure TfrmMain.Button2Click(Sender: TObject);
 *  begin
 *    if FWebSocket.Connected then
 *      FWebSocket.Send('Hello, WebSocket !');
 *  end;
 *
 *  procedure TfrmMain.WebSocketEstablished(Sender: TObject);
 *  begin
 *    Memo1.Lines.Add('Estalished !');
 *  end;
 *
 *  procedure TfrmMain.WebSocketText(Sender: TObject; const iText: String);
 *  begin
 *    Memo1.Lines.Add('RECEIVED: ' + iText);
 *  end;
 *
 *  // Error Handler
 *  procedure TfrmMain.WebSocketError(
 *    Sender: TObject;
 *    const AErrorCode: Integer; // The error code is defined on line 169.
 *    const AException: Exception);
 *  begin
 *    Memo1.Lines.Add(Format('ERROR: %d  %s', [AErrCode, AException.Message]));
 *  end;
 *)

unit PK.Net.WebSocket;

//{$DEFINE NEED_LOG}

{$IFDEF NEED_LOG}
  {$DEFINE NEED_RAW_LOG}
{$ENDIF}

interface

uses
  System.Classes
  , System.SysUtils
  , System.Types
  , System.Net.URLClient
  , IdTCPClient
  , IdIOHandler
  , IdSSLOpenSSL
  ;

type
  TWebSocket = class(TComponent)
  public type
    TOpCode = (
      Noop = -1,
      Continuation = $0,
      Text =         $1,
      Binary =       $2,
      Close =        $8,
      Ping =         $9,
      Pong =         $A);

    TErrorEvent =
      procedure (
        Sender: TObject;
        const iErrCode: Integer;
        const iMessage: String) of object;
    TTextEvent = procedure (Sender: TObject; const iText: String) of object;
    TBinaryEvent =
      procedure (Sender: TObject; const iData: TBytes) of object;
    TSendReceiveEvent =
      procedure (
        Sender: TObject;
        const iData: TBytes;
        const iCode: TOpCode) of object;
    TCloseEvent =
      procedure (
        Sender: TObject;
        const iCloseCode: Integer;
        const iData: TBytes) of object;
  public const
    OP_CODE_TEXT: array [TOpCode] of String = (
      'Noop',
      'Continuation',
      'Text',
      'Binary',
      'Reserved1',
      'Reserved2',
      'Reserved3',
      'Reserved4',
      'Reserved5',
      'Close',
      'Ping',
      'Pong');

    CLOSE_CODE_NORMALY = 1000;
    CLOSE_CODE_DISAPPEARED = 1001;
    CLOSE_CODE_PROTOCOL_ERROR = 1002;
    CLOSE_CODE_NOT_SUPPORTED = 1003;
    CLOSE_CODE_REGISTED = 1004;
    CLOSE_CODE_STATUS_NOT_RECEIVED = 1005;
    CLOSE_CODE_ILLEGAL_CLOSURE = 1006;
    CLOSE_CODE_ILLEGAL_PAYLOAD_DATA = 1007;
    CLOSE_CODE_POLICY_ERROR = 1008;
    CLOSE_CODE_HUGE_MESSAGE = 1009;
    CLOSE_CODE_REQUIRED_EXTENSIONS = 1010;
    CLOSE_CODE_INTERNAL_SERVER_ERROR = 1011;
    CLOSE_CODE_TLS_HANDSHAKE_FALED = 1015;

    ERR_CODE_CONNECTION = 1;
    ERR_CODE_HANDSHAKE_TIMEOUT = 2;
    ERR_CODE_INVALID_RESPONCE = 3;
    ERR_CODE_HANDSHAKE = 4;
    ERR_CODE_SEND = 5;
  protected const
    CR = #13;
    LF = #10;
    EOL = #13#10;
    DEF_HANDSHAKE_TIMEOUT = 30 * 1000; // [msec]
    DEF_HEARTBEAT_INTERVAL = 30 * 1000; // [msec]
    DEF_USER_AGENT = 'Delphi_PK_WebSocket_Client';
  protected type
    TCallEventType = (
      Error,
      Established,
      Closed,
      Send,
      Receive,
      OpText,
      OpBinary,
      HeartBeat);
  private type
    TSentFrameProc = reference to procedure (const iCompleted: Boolean);

    TIndySocket = class(TComponent)
    private const
      INDY_SOCKET_READ_WAIT_TIME = 500; // [msec]
    private var
      FOwner: TWebSocket;
      FSocket: TIdTCPClient;
      FHandler: TIdIOHandler;
    private
      function CheckAvailable: Boolean;
      function GetConnected: Boolean;
    public
      constructor Create(const iOwner: TWebSocket); reintroduce;
      destructor Destroy; override;
      procedure Connect(const iURI: TURI);
      procedure Close;
      procedure Send(
        const iBytes: TBytes;
        const iProc: TWebSocket.TSentFrameProc);
      function Fetch: Integer;
      procedure Receive(var iBytes: TBytes);
      property Connected: Boolean read GetConnected;
    end;

    TSocketThread = class(TThread)
    private type
      TState = (
        None,
        TryConnect,
        TryHandshake,
        WaitHandshake,
        Waiting,
        TryClose,
        SocketClosed);
    private const
      DEF_SLEEP_TIME = 50;
      MSG_ERR_CONNECTION = 'Connection Failed.';
      MSG_ERR_HANDSHAKE_TIMEOUT = 'Handshake session time out.';
      MSG_ERR_INVALID_RESPONCE = 'Invalid HTTP Responce.';
      MSG_ERR_HANDSHAKE = 'WebSocket Handshake Failed.';
      MSG_ERR_SEND = 'Socket send Failed.';
    private var
      FRunning: Boolean;
      FOwner: TWebSocket;
      FSecWebSocketResponse: String;
      FSocket: TWebSocket.TIndySocket;
      FState: TState;
      {$IFDEF NEED_LOG}FPrevState: TState;{$ENDIF}
      FScheme: String;
      FHost: String;
      FPath: String;
      FRaw: TBytes;
      FFrame: TBytes;
      FHandshakeStartTime: TDateTime;
      FHandshaked: Boolean;
      FHeartBeatPrev: TDateTime;
      FCloseCode: Integer;
      FCloseBytes: TBytes;
      FDestroying: Boolean;
    private
      function MSecBetween(
        const iOld: TDateTime;
        const iNow: TDateTime = 0): Integer;
      function GenerateWebSocketKey: String;
      procedure StartHandshake;
      function WaitHandshake: Boolean;
      function GetReceivedData(const iDelim: String): TBytes;
      procedure SendFrame(
        const iData: TBytes;
        const iCode: TOpCode;
        const iProc: TSentFrameProc = nil);
      procedure SendTextFrame(
        const iStr: String;
        const iCode: TOpCode;
        const iProc: TSentFrameProc = nil);
      procedure SendOpCode(
        const iCode: TOpCode;
        const iBin: TBytes;
        const iProc: TSentFrameProc = nil); overload;
      procedure SendOpCode(
        const iCode: TOpCode;
        const iParam: Word;
        const iProc: TSentFrameProc = nil); overload;
      procedure SendOpCode(
        const iCode: TOpCode;
        const iProc: TSentFrameProc = nil); overload;
      procedure ReceiveFrame;
      procedure CheckHeartBeat;
      procedure CloseImmidiate;
      function BytesToString(
        const iBytes: TBytes;
        const iForce: Boolean = True): String;
      {$IFDEF NEED_LOG}
      function BytesToHex(const iBytes: TBytes): String;
      {$ENDIF}
    public
      class function EncodeFrame(
        const iData: TBytes;
        const iCode: TOpCode;
        const iMask: Boolean = True): TBytes;
      class function DecodeFrame(
        const iData: TBytes;
        out oFrameSIze: Integer;
        out oFIN: Boolean;
        out oOpCode: TOpCode;
        out oResult: TBytes): Boolean;
    public
      constructor Create(const iOwner: TWebSocket); reintroduce;
      destructor Destroy; override;
      procedure Execute; override;
    public
      function Connect: Boolean;
      procedure Close;
      procedure SendText(const iText: String);
      procedure SendBinary(const iBinary: TBytes);
    end;
  private var
    FURL: String;
    FUserAgent: String;
    FSocketThread: TSocketThread;
    FHandshakeTimeout: Integer;
    FHeartBeatInterval: Integer;
    FAsyncEvent: Boolean;
    FOnError: TErrorEvent;
    FOnClosed: TCloseEvent;
    FOnEstablished: TNotifyEvent;
    FOnText: TTextEvent;
    FOnBinary: TBinaryEvent;
    FOnReceive: TSendReceiveEvent;
    FOnSend: TSendReceiveEvent;
    FOnHeartBeat: TNotifyEvent;
  private
    function GetConnected: Boolean;
  protected
    procedure CallEventSub(
      const iType: TCallEventType;
      const iCloseCode: Integer;
      const iText: String;
      const iData: TBytes;
      const iCode: TOpCode);
    procedure CallEvent(
      const iType: TCallEventType;
      const iCloseCode: Integer;
      const iText: String;
      const iData: TBytes;
      const iCode: TOpCode);
    procedure CallOnError(const iErrCode: Integer; const iText: String);
    procedure CallOnEstablished;
    procedure CallOnClose(const iCloseCode: Integer; const iData: TBytes);
    procedure CallOnSend(
      const iData: TBytes;
      const iCode: TOpCode);
    procedure CallOnReceive(
      const iData: TBytes;
      const iCode: TOpCode);
    procedure CallOnText(const iText: String);
    procedure CallOnBinary(const iData: TBytes);
    procedure CallOnHeartBeat;
  public
    constructor Create(iOwner: TComponent); override;
    destructor Destroy; override;
  public
    procedure Connect(const iURL: String);
    procedure Close;
    procedure Send(const iText: String); overload;
    procedure Send(const iData: TBytes); overload;
    procedure SendPing;
    procedure ResetHeartbeat;
  public
    class function DecodeFrame(
      const iData: TBytes;
      out oFrameSize: Integer;
      out oFIN: Boolean;
      out oOpCode: TOpCode;
      out oResult: TBytes): Boolean;
  public
    property URL: String read FURL;
    property Connected: Boolean read GetConnected;
  published
    property AsyncEvent: Boolean
      read FAsyncEvent write FAsyncEvent default True;
    property UserAgent: String read FUserAgent write FUserAgent;
    property HandshakeTimeout: Integer
      read FHandshakeTimeout write FHandshakeTimeout;
    property HeartBeatInterval: Integer
      read FHeartBeatInterval write FHeartBeatInterval;
  published
    property OnError: TErrorEvent read FOnError write FOnError;
    property OnEstablished: TNotifyEvent
      read FOnEstablished write FOnEstablished;
    property OnClosed: TCloseEvent read FOnClosed write FOnClosed;
    property OnSend: TSendReceiveEvent read FOnSend write FOnSend;
    property OnReceive: TSendReceiveEvent read FOnReceive write FOnReceive;
    property OnText: TTextEvent read FOnText write FOnText;
    property OnBinary: TBinaryEvent read FOnBinary write FOnBinary;
    property OnHeartBeat: TNotifyEvent read FOnHeartBeat write FOnHeartBeat;
  end;

implementation

uses
  System.Hash
  , System.NetEncoding
  , System.DateUtils
  , IdGlobal
  {$IFNDEF LINUX}
    {$IFDEF NEED_LOG}
    , PK.Utils.Log
    {$ENDIF}
  {$ENDIF}
  ;

{$IFDEF NEED_LOG}
procedure Log(const iLog: String);
begin
  {$IFDEF LINUX}
  Writeln(FormatDateTime('yyyy/mm/dd hh:nn:ss:zz', Now), ' ', iLog);
  {$ELSE}
  PK.Utils.Log.Log.d(iLog);
  {$ENDIF}
end;
{$ENDIF}

{ TWebSocket.TIndySocket }

function TWebSocket.TIndySocket.CheckAvailable: Boolean;
begin
  Result :=
    (FSocket <> nil) and
    (FHandler <> nil) and
    (FSocket.IOHandler <> nil) and
    (FSocket.Socket <> nil);

  if Result and (FHandler is TIdSSLIOHandlerSocketOpenSSL) then
    Result := (TIdSSLIOHandlerSocketOpenSSL(FHandler).SSLSocket <> nil);
end;

procedure TWebSocket.TIndySocket.Close;
begin
  if GetConnected then
  begin
    try
      FSocket.Disconnect
    except on E: Exception do
      {$IFDEF NEED_LOG}
      Log('TIndySocket.Close.Error: ' + E.Message);
      {$ENDIF}
    end
  end;
end;

procedure TWebSocket.TIndySocket.Connect(const iURI: TURI);
begin
  try
    if FSocket <> nil then
    begin
      Close;
      FSocket.DisposeOf;
      FSocket := nil;
    end;

    if FHandler <> nil then
    begin
      FHandler.Close;
      FHandler := nil;
    end;

    if FHandler = nil then
    begin
      if iURI.Scheme = TURI.SCHEME_HTTPS then
      begin
        FHandler := TIdSSLIOHandlerSocketOpenSSL.Create(Self);
        with TIdSSLIOHandlerSocketOpenSSL(FHandler) do
        begin
          SSLOptions.SSLVersions := [sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];
          SSLOptions.Mode := TIdSSLMode.sslmClient;
        end;
      end;
    end;

    FSocket := TIdTCPClient.Create(Self);
    FSocket.Host := iURI.Host;
    FSocket.Port := iURI.Port;

    FSocket.IOHandler := FHandler;

    FSocket.Connect;
    if FHandler = nil then
      FHandler := FSocket.IOHandler;
  except
    on E: Exception do
    begin
      FOwner.CallOnError(ERR_CODE_CONNECTION, E.Message);
      {$IFDEF NEED_LOG}
      Log('TIndySocket.Connect.Error: ' + E.Message);
      {$ENDIF}
    end;
  end;
end;

constructor TWebSocket.TIndySocket.Create(const iOwner: TWebSocket);
begin
  inherited Create(iOwner);

  FOwner := iOwner;
end;

destructor TWebSocket.TIndySocket.Destroy;
begin
  Close;
  inherited;
end;

function TWebSocket.TIndySocket.Fetch: Integer;
begin
  Result := 0;

  if (not CheckAvailable) or (not FHandler.Connected) then
    Exit;

  FSocket.IOHandler.CheckForDataOnSource(INDY_SOCKET_READ_WAIT_TIME);
  Result := FSocket.IOHandler.InputBuffer.Size;
end;

function TWebSocket.TIndySocket.GetConnected: Boolean;
begin
  if CheckAvailable then
    Result := FSocket.Connected
  else
    Result := False;
end;

procedure TWebSocket.TIndySocket.Receive(var iBytes: TBytes);
begin
  if not CheckAvailable then
  begin
    FOwner.FSocketThread.FState := TSocketThread.TState.None;
    Exit;
  end;

  // Fetch してからじゃないと Indy のデータは取れない
  SetLength(iBytes, FSocket.IOHandler.InputBuffer.Size);
  FHandler.ReadBytes(TIdBytes(iBytes), Length(iBytes), False);
end;

procedure TWebSocket.TIndySocket.Send(
  const iBytes: TBytes;
  const iProc: TWebSocket.TSentFrameProc);
begin
  if not CheckAvailable then
  begin
    FOwner.FSocketThread.FState := TSocketThread.TState.None;
    Exit;
  end;

  try
    FHandler.WriteDirect(TIdBytes(iBytes));

    if Assigned(iProc) then
      iProc(True);
  except on E: Exception do
    FOwner.CallOnError(ERR_CODE_SEND, E.Message);
  end;
end;

{ TWebSocket.TSocketThread }

{$IFDEF NEED_LOG}
function TWebSocket.TSocketThread.BytesToHex(const iBytes: TBytes): String;
begin
  var SB := TStringBuilder.Create;
  try
    for var B in iBytes do
    begin
      SB.Append(B.ToHexString);
      SB.Append(' ');
    end;

    Result := SB.ToString;
  finally
    SB.DisposeOf;
  end;
end;
{$ENDIF}

function TWebSocket.TSocketThread.BytesToString(
  const iBytes: TBytes;
  const iForce: Boolean = True): String;
begin
  Result := '';

  try
    Result := TEncoding.UTF8.GetString(iBytes);
  except on E: Exception do
    Result := '';
  end;

  if Result.IsEmpty and iForce then
    try
      Result := TEncoding.ANSI.GetString(iBytes);
    except on E: Exception do
      Result := '';
    end;
end;

procedure TWebSocket.TSocketThread.CheckHeartBeat;
begin
  var Current := Now;
  var MSec := MSecBetween(FHeartBeatPrev, Current) + DEF_SLEEP_TIME * 2;

  if FOwner.FHeartBeatInterval <= MSec then
  begin
    FHeartBeatPrev := Current;

    if Assigned(FOwner.FOnHeartBeat) then
      FOwner.CallOnHeartBeat
    else
      SendOpCode(TOpCode.Ping);
  end;
end;

procedure TWebSocket.TSocketThread.Close;
begin
  if Terminated then
    CloseImmidiate;

  if (FState > TState.TryConnect) and (FHandshaked) then
    FState := TState.TryClose;
end;

procedure TWebSocket.TSocketThread.CloseImmidiate;
begin
  if (not FDestroying) and (FSocket.Connected) then
  begin
    FCloseCode := 0;
    SetLength(FCloseBytes, 0);

    SendOpCode(
      TOpCode.Close,
      CLOSE_CODE_NORMALY,
      procedure(const iCompleted: Boolean)
      begin
        {$IFDEF NEED_LOG}
          Log('Close sent. Completed = ' + Ord(iCompleted).ToString);
        {$ENDIF}

        FSocket.Close;
      end
    );
  end;
end;

function TWebSocket.TSocketThread.Connect: Boolean;
begin
  Result := False;

  var URL := FOwner.FURL.Replace('ws', 'http', []); // ws -> http, wss -> https

  var URI := TURI.Create(URL);
  if URI.Path.IsEmpty  then
    URI.Path := '/';

  var Q := URI.Query;
  if not Q.IsEmpty then
    Q := '?' + Q;

  FScheme := URI.Scheme;
  FHost := URI.Host;
  FPath := URI.Path + Q;

  try
    if FSocket <> nil then
      FSocket.DisposeOf;

    FSocket := TWebSocket.TIndySocket.Create(FOwner);
    FSocket.Connect(URI);

    Result := True;
  except on E: Exception do
    FOwner.CallOnError(ERR_CODE_CONNECTION, E.Message);
  end;
end;

constructor TWebSocket.TSocketThread.Create(const iOwner: TWebSocket);
begin
  FOwner := iOwner;
  FState := TState.None;

  inherited Create(False);
end;

class function TWebSocket.TSocketThread.DecodeFrame(
  const iData: TBytes;
  out oFrameSize: Integer;
  out oFIN: Boolean;
  out oOpCode: TOpCode;
  out oResult: TBytes): Boolean;
var
  Index: UInt32;
  MaskingKey: UInt32;
  MaskingKyeArray: array [0.. 3] of Byte absolute MaskingKey;
  i: UInt32;
begin
  Result := False;
  oFrameSize := 0;

  var DataLen := Length(iData);
  if DataLen < 2 then
    Exit;

  // FIN & OpCode
  oFIN := (iData[0] and $80) shr 7 > 0;
  oOpCode := TOpCode(iData[0] and $0f);

  // Mask
  var Mask := (iData[1] and $80) shr 7 > 0;

  // PayloadLen
  var PayloadLen: UInt64;
  var PayloadLenRaw: Int64Rec;
  var Len := iData[1] and $7f;
  var Size: UInt32 := 2;

  UInt64(PayloadLenRaw) := 0;

  Index := 2;

  if Len < $7e then
    PayloadLen := Len
  else
  begin
    var PayloadLenCount: UInt32;

    if Len = $7e then
      PayloadLenCount := 2
    else
      PayloadLenCount := 8;

    var DataIndex := Index + PayloadLenCount - 1;

    for i := 0 to PayloadLenCount - 1 do
      PayloadLenRaw.Bytes[i] := iData[DataIndex - i];

    PayloadLen := UInt64(PayloadLenRaw);

    Inc(Index, PayloadLenCount);
    Inc(Size, PayloadLenCount);
  end;

  oFrameSize := Integer(Size + UInt32(Ord(Mask) * 4) + PayloadLen);

  if DataLen < oFrameSize then
    Exit(oOpCode in [TOpCode.Close, TOpCode.Ping, TOpCode.Pong]);

  // Masking Key
  MaskingKey := 0;
  if Mask then
  begin
    Move(iData[Index], MaskingKey, SizeOf(MaskingKey));
    Inc(Index, SIzeOf(MaskingKey));
  end;

  // Payload Data
  if PayloadLen > 0 then // 10.3 Rio's bug. PayloadLen = 0 でも for に入る
  begin
    SetLength(oResult, PayloadLen);
    for i := 0 to PayloadLen - 1 do
      oResult[i] := iData[Index + i] xor MaskingKyeArray[i mod 4];
  end;

  Result := True;
end;

destructor TWebSocket.TSocketThread.Destroy;
begin
  if FRunning then
  begin
    Close;
    FDestroying := True;
  end
  else
  begin
    inherited;
    FSocket.DisposeOf;
  end;
end;

class function TWebSocket.TSocketThread.EncodeFrame(
  const iData: TBytes;
  const iCode: TOpCode;
  const iMask: Boolean = True): TBytes;
var
  Index: Integer;
  MaskingKey: UInt32;
  MaskingKyeArray:array [0.. 3] of Byte absolute MaskingKey;
  i: Integer;

  procedure SetBuff(const iValue: Byte);
  begin
    Result[Index] := iValue;
    Inc(Index);
  end;

begin
  // CalcBuffSize
  var Len: UInt64 := UInt64(Length(iData));
  var ExPayloadLen: UInt32  := 0;
  var MaskLenValue: UInt32;
  var PayloadLen: Int64Rec;

  if Len < 126 then
    MaskLenValue := Len
  else
  begin
    if Len < $10000 then
    begin
      ExPayloadLen := 2;
      MaskLenValue := 126;
      PayloadLen.Lo := Len
    end
    else
    begin
      ExPayloadLen := 8;
      MaskLenValue := 127;
      PayloadLen := Int64Rec(Len);
    end;
  end;

  SetLength(Result, 1 + 1 + ExPayloadLen + UInt32(Ord(iMask) * 4) + Len);
  Index := 0;

  // 0 Byte
  // Final Flag. String Length を 32bit で考えているので 1 Frame で収まる
  var FIN := 1;
  SetBuff(FIN shl 7 + Ord(iCode));

  // 1 Byte
  SetBuff(MaskLenValue + UInt32(Ord(iMask) shl 7));

  // 2 ～ 10 Byte
  // Payload Length
  for i := ExPayloadLen - 1 downto 0 do
    SetBuff(PayloadLen.Bytes[i]);

  // Masking Key
  MaskingKey := Random(Int32($ffffffff));

  Move(MaskingKey, Result[Index], SizeOf(MaskingKey));
  Inc(Index, SizeOf(MaskingKey));

  // Payload Data
  for i := 0 to Len - 1 do // 10.3.1 のバグで i をインライン定義するとエラー
    SetBuff(Ord(iData[i]) xor MaskingKyeArray[i mod 4]);
end;

procedure TWebSocket.TSocketThread.Execute;

  procedure Wait;
  begin
    TThread.Sleep(DEF_SLEEP_TIME);
  end;

  procedure Finalize;
  begin
    FHandshaked := False;
    FState := TState.None;
  end;

begin
  {$IFDEF NEED_LOG}
  FPrevState := TState.None;
  {$ENDIF}

  FRunning := True;
  try
    while (not Terminated) and (not FDestroying) do
    begin
      {$IFDEF NEED_LOG}
      if FPrevState <> FState then
      begin
        Log('Thread, FState = ' + Ord(FState).ToString);
        FPrevState := FState;
      end;
      {$ENDIF}

      case FState of
        TState.None:
        begin
          if FSocket.Connected then
            Wait
          else
          begin
            FSocket.Close;
            Break;
          end;
        end;

        TState.TryConnect:
        begin
          if Connect then // この中で Socket を作成
            FState := TState.TryHandshake
          else
            FState := TState.None;
        end;

        TState.TryHandshake:
        begin
          StartHandshake;
          FHandshakeStartTime := Now;
          FState := TState.WaitHandshake;
        end;

        TState.WaitHandshake:
        begin
          if WaitHandshake then
          begin
            FCloseCode := 0;
            SetLength(FCloseBytes, 0);
            FHeartBeatPrev := Now;
            FState := TState.Waiting
          end
          else
          begin
            {$IFDEF NEED_LOG}
              var b := MSecBetween(FHandshakeStartTime);
              Log(Format('Handshake waiting... ( %d [msec] )', [b]));
            {$ENDIF}

            if
              FOwner.FHandshakeTimeout < MSecBetween(FHandshakeStartTime)
            then
            begin
              {$IFDEF NEED_LOG}
                Log('Handshake timeout');
              {$ENDIF}

              FSocket.Close;
              FState := TState.None;
              FOwner.CallOnError(
                ERR_CODE_HANDSHAKE_TIMEOUT,
                MSG_ERR_HANDSHAKE_TIMEOUT);
            end
            else
              Wait;
          end;
        end;

        TState.Waiting:
        begin
          if FSocket.Connected then
          begin
            CheckHeartBeat;
            ReceiveFrame;
            Wait;
          end
          else
            Finalize;
        end;

        TState.TryClose:
        begin
          CloseImmidiate;
          FState := TState.SocketClosed;
        end;

        TState.SocketClosed:
        begin
          Finalize;
          Wait;
          Break;
        end;
      end;
    end;
  finally
    FRunning := False;
  end;

  FOwner.CallOnClose(FCloseCode, FCloseBytes);

  // ここで Socket を破棄
  if FDestroying then
    FSocket.DisposeOf;

  FSocket := nil;

  if FDestroying then
    inherited Destroy;
end;

function TWebSocket.TSocketThread.GenerateWebSocketKey: String;
var
  Rands: TBytes;
begin
  SetLength(Rands, 16);
  for var i := Low(Rands) to High(Rands) do
    Rands[i] := Byte(Random(255));

  Result := TNetEncoding.Base64.EncodeBytesToString(Rands);

  FSecWebSocketResponse :=
    Result +
    '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'; // The GUID by RFC6455

  var Hash := THashSHA1.GetHashBytes(FSecWebSocketResponse);

  FSecWebSocketResponse := TNetEncoding.Base64.EncodeBytesToString(Hash);
end;

function TWebSocket.TSocketThread.GetReceivedData(const iDelim: String): TBytes;
var
  Bytes: TBytes;
  Index: Integer;
  Len: Integer;
begin
  if FState >= TState.TryClose then
    Exit;

  var Count := FSocket.Fetch;
  if Count < 0 then
    Exit;

  FSocket.Receive(Bytes);
  if Length(Bytes) < 1 then
    Exit;

  FRaw := FRaw + Bytes;

  {$IFDEF NEED_RAW_LOG}
  Log('');
  Log('RAW DATA --------------------');
  try
    Log(BytesToHex(Bytes));

    var Str: String := BytesToString(Bytes, False);

    if not Str.IsEmpty then
    begin
      Log('TO_UTF8 ---------------------');
      Log(Str);
    end;
  except
    on E: Exception do
    begin
      Log('ERROR!! RAW DATA');
      Log(E.Message);
    end;
  end;
  Log('-----------------------------');
  Log('');
  {$ENDIF}

  if iDelim.IsEmpty then
  begin
    Result := FRaw;
    SetLength(FRaw, 0);
  end
  else
  begin
    var RawStr := BytesToString(FRaw);
    Index := RawStr.IndexOf(iDelim);

    if Index < 0 then
      SetLength(Result, 0)
    else
    begin
      Len := Index + iDelim.Length;
      Result := TEncoding.ANSI.GetBytes(RawStr.SubString(0, Len));
      FRaw := TEncoding.ANSI.GetBytes(RawStr.SubString(Len));
    end;
  end;
end;

function TWebSocket.TSocketThread.MSecBetween(
  const iOld: TDateTime;
  const iNow: TDateTime = 0): Integer;
begin
  var Current := iNow;
  if Current = 0 then
    Current := Now;

  Result := MilliSecondsBetween(Current, iOld);
end;

procedure TWebSocket.TSocketThread.ReceiveFrame;
var
  Bytes: TBytes;
begin
  FFrame := FFrame + GetReceivedData('');

  var FIN: Boolean;
  var OpCode: TOpCode;
  var Size: Integer;

  if
    not TWebSocket.TSocketThread.DecodeFrame(FFrame, Size, FIN, OpCode, Bytes)
  then
    Exit;

  var Len := Length(FFrame) - Size;
  Move(FFrame[Size], FFrame[0], Len);
  SetLength(FFrame, Len);

  FOwner.CallOnReceive(Bytes, OpCode);

  case OpCode of
    TOpCode.Continuation:
    begin
    end;

    TOpCode.Text:
    begin
      var Data: String := BytesToString(Bytes);

      {$IFDEF NEED_LOG}
      Log('RECEIVED: ' + Data);
      if (Data.IsEmpty) and (Length(Bytes) > 0) then
        Log('RECEIVED Data is can not decode: ' + BytesToHex(Bytes));
      {$ENDIF}

      FOwner.CallOnText(Data);
    end;

    TOpCode.Binary:
    begin
      {$IFDEF NEED_LOG}
      Log('RECEIVED: BINARY');

      var Body := BytesToString(Bytes);

      if Body.IsEmpty then
        Log(BytesToHex(Bytes))
      else
        Log(Body);
      {$ENDIF}

      FOwner.CallOnBinary(Bytes);
    end;

    TOpCode.Close:
    begin
      {$IFDEF NEED_LOG}
      Log('RECEIVED: CLOSE');
      {$ENDIF}

      FCloseCode := 0;
      FCloseBytes := Bytes;

      var BytesLen := Length(FCloseBytes);
      if BytesLen > 1 then
      begin
        FCloseCode := FCloseBytes[0] shl 8 + FCloseBytes[1];

        Dec(BytesLen, 2);
        Move(FCloseBytes[2], FCloseBytes[0], BytesLen);
        SetLength(FCloseBytes, BytesLen);
      end;

      FState := TState.SocketClosed;
      //Close;
    end;

    TOpCode.Ping:
    begin
      {$IFDEF NEED_LOG}
      Log('RECEIVED: PING');
      {$ENDIF}
      SendOpCode(TOpCode.Pong)
    end;

    TOpCode.Pong:
    begin
      {$IFDEF NEED_LOG}
      Log('RECEIVED: PONG');
      {$ENDIF}
      // Nothing to do
    end;
  end;
end;

procedure TWebSocket.TSocketThread.SendBinary(const iBinary: TBytes);
begin
  SendFrame(iBinary, TOpCode.Binary);
end;

procedure TWebSocket.TSocketThread.SendFrame(
  const iData: TBytes;
  const iCode: TOpCode;
  const iProc: TSentFrameProc = nil);
begin
  if FSocket.Connected then
  begin
    var Bytes := TWebSocket.TSocketThread.EncodeFrame(iData, iCode);

    FOwner.CallOnSend(iData, iCode);

    try
      FSocket.Send(
        Bytes,
        procedure(const iCompleted: Boolean)
        begin
          if Terminated then
            Exit;

          {$IFDEF NEED_LOG}
          if iCompleted then
          begin
            var FIN: Boolean;
            var OpCode: TOpCode;
            var Size: Integer;
            var Bytes2: TBytes;

            if
              TWebSocket.TSocketThread.DecodeFrame(
                Bytes,
                Size,
                FIN,
                OpCode,
                Bytes2)
            then
            begin
              if OpCode = TWebSocket.TOpCode.Text then
                Log(
                  'SEND: OpCode = ' + Ord(OpCode).ToString +
                  ', Body = ' + BytesToString(Bytes2))
              else
              begin
                var Body := BytesToHex(Bytes2);
                Log(
                  'SEND: OpCode = ' + Ord(OpCode).ToString +
                  ', BodyLen = ' + Body.Length.ToString +
                  ', Body = [' + Body + ']');
              end;
            end
            else
              Log('SEND: Parse Error');
          end
          else
          begin
            Log('SEND: FALAED');
            Log(BytesToString(Bytes));
          end;
          {$ENDIF}

          if Assigned(iProc) then
            iProc(iCompleted);
        end
      );
    except
      on E: Exception do
        if FState < TState.TryClose then
          FOwner.CallOnText(E.Message);
    end;
  end;
end;

procedure TWebSocket.TSocketThread.SendOpCode(
  const iCode: TOpCode;
  const iProc: TSentFrameProc);
begin
  SendOpCode(iCode, 0, iProc);
end;

procedure TWebSocket.TSocketThread.SendOpCode(
  const iCode: TOpCode;
  const iParam: Word;
  const iProc: TSentFrameProc);
var
  Bytes: TBytes;
begin
  SetLength(Bytes, SizeOf(iParam));
  Bytes[0] := WordRec(iParam).Hi;
  Bytes[1] := WordRec(iParam).Lo;

  SendOpCode(iCode, Bytes, iProc);
end;

procedure TWebSocket.TSocketThread.SendOpCode(
  const iCode: TOpCode;
  const iBin: TBytes;
  const iProc: TSentFrameProc);
begin
  SendFrame(iBin, iCode, iProc);
end;

procedure TWebSocket.TSocketThread.SendText(const iText: String);
begin
  SendTextFrame(iText, TOpCode.Text);
end;

procedure TWebSocket.TSocketThread.SendTextFrame(
  const iStr: String;
  const iCode: TOpCode;
  const iProc: TSentFrameProc = nil);
begin
  SendFrame(TEncoding.UTF8.GetBytes(iStr), iCode, iProc);
end;

procedure TWebSocket.TSocketThread.StartHandshake;

  procedure SendLn(const iStr: String);
  begin
    {$IFDEF NEED_LOG}
    Log('HEADER: ' + iStr);
    {$ENDIF}
    FSocket.Send(TEncoding.UTF8.GetBytes(iStr + EOL), nil);
  end;

  procedure SendLnFormat(
    const iFormat: String;
    const iStrs: array of const);
  begin
    SendLn(Format(iFormat, iStrs));
  end;

begin
  FHandshaked := False;

  // Do not localize
  SendLnFormat('GET %s HTTP/1.1', [FPath]);
  SendLnFormat('Host: %s', [FHost]);
  SendLn('Connection: Upgrade');
  SendLn('Pragma: no-cache');
  SendLn('Cache-Control: no-cache');
  SendLnFormat('User-Agent: %s', [FOwner.FUserAgent]);
  SendLn('Upgrade: websocket');
  SendLnFormat('Origin: %s://%s', [FScheme, FHost]);
  SendLn('Sec-WebSocket-Version: 13');
  SendLnFormat('Sec-WebSocket-Key: %s', [GenerateWebSocketKey]);
  SendLn('');
end;

function TWebSocket.TSocketThread.WaitHandshake: Boolean;
begin
  if FHandshaked then
    Exit(True);

  Result := False;

  var Data := GetReceivedData(EOL + EOL);

  var Len := Length(Data);
   if Len < 1 then
    Exit;

  var SL := TStringList.Create;
  try
    SL.NameValueSeparator := ':';

    SL.Text := BytesToString(Data);

    {$IFDEF NEED_LOG}
    Log('TWebSocket.TSocketThread.WaitHandshake.ReceivedData: ' + SL.Text);
    {$ENDIF}


    // Do not localize

    if (SL.Count < 1) or (not SL[0].StartsWith('HTTP/1.1 101')) then

    begin
      FOwner.CallOnError(ERR_CODE_INVALID_RESPONCE, MSG_ERR_INVALID_RESPONCE);
      Exit;
    end;

    var Connection := SL.Values['Connection'].Trim.ToLower;
    var Upgrade := SL.Values['Upgrade'].Trim.ToLower;
    var SecWebSocketAccept := SL.Values['Sec-WebSocket-Accept'].Trim;

    if (Connection = 'upgrade') and (Upgrade = 'websocket') then
    begin
      if
        (SecWebSocketAccept.IsEmpty) or
        (SecWebSocketAccept = FSecWebSocketResponse)
      then
        FHandshaked := True
      else
        FOwner.CallOnError(ERR_CODE_HANDSHAKE, MSG_ERR_HANDSHAKE);
    end;
  finally
    SL.DisposeOf;
  end;

  Result := FHandshaked;

  if Result then
    FOwner.CallOnEstablished;
end;

{ TWebSocket }

procedure TWebSocket.CallEvent(
  const iType: TCallEventType;
  const iCloseCode: Integer;
  const iText: String;
  const iData: TBytes;
  const iCode: TOpCode);
begin
  if IsConsole then
  begin
    if iType = TCallEventType.Error then
      CallEventSub(iType, iCloseCode, iText, iData, iCode)
    else
      TThread.CreateAnonymousThread(
        procedure
        begin
          CallEventSub(iType, iCloseCode, iText, iData, iCode)
        end
      ).Start;
  end
  else
  begin
    if FAsyncEvent then
      CallEventSub(iType, iCloseCode, iText, iData, iCode)
    else
      TThread.Queue(
        TThread.Current,
        procedure
        begin
          CallEventSub(iType, iCloseCode, iText, iData, iCode);
        end
      );
  end;
end;

procedure TWebSocket.CallEventSub(
  const iType: TCallEventType;
  const iCloseCode: Integer;
  const iText: String;
  const iData: TBytes;
  const iCode: TOpCode);
begin
  case iType of
    TCallEventType.Error:
      if Assigned(FOnError) then
        FOnError(Self, iCloseCode, iText);

    TCallEventType.Established:
      if Assigned(FOnEstablished) then
        FOnEstablished(Self);

    TCallEventType.Closed:
      if Assigned(FOnClosed) then
        FOnClosed(Self, iCloseCode, iData);

    TCallEventType.Send:
      if Assigned(FOnSend) then
        FOnSend(Self, iData, iCode);

    TCallEventType.Receive:
      if Assigned(FOnReceive) then
        FOnReceive(Self, iData, iCode);

    TCallEventType.OpText:
      if Assigned(FOnText) then
        FOnText(Self, iText);

    TCallEventType.OpBinary:
      if Assigned(FOnBinary) then
        FOnBinary(Self, iData);

    TCallEventType.HeartBeat:
      if Assigned(FOnHeartBeat) then
        FOnHeartBeat(Self);
  end;
end;

procedure TWebSocket.CallOnBinary(const iData: TBytes);
begin
  CallEvent(TCallEventType.OpBinary, 0, '', iData, TOpCode.Binary);
end;

procedure TWebSocket.CallOnClose(
  const iCloseCode: Integer;
  const iData: TBytes);
begin
  CallEvent(TCallEventType.Closed, iCloseCode, '', iData, TOpCode.Noop);
end;

procedure TWebSocket.CallOnError(const iErrCode: Integer; const iText: String);
begin
  {$IFDEF NEED_LOG}
  Log(Format('TWebSocket.CallOnError: %d,  %s', [iErrCode, iText]));
  {$ENDIF}

  CallEvent(TCallEventType.Error, iErrCode, iText, nil, TOpCode.Text);
end;

procedure TWebSocket.CallOnEstablished;
begin
  CallEvent(TCallEventType.Established, 0, '', nil, TOpCode.Noop);
end;

procedure TWebSocket.CallOnHeartBeat;
begin
  CallEvent(TCallEventType.HeartBeat, 0, '', nil, TOpCode.Noop);
end;

procedure TWebSocket.CallOnReceive(const iData: TBytes; const iCode: TOpCode);
begin
  CallEvent(TCallEventType.Receive, 0, '', iData, iCode);
end;

procedure TWebSocket.CallOnSend(const iData: TBytes; const iCode: TOpCode);
begin
  CallEvent(TCallEventType.Send, 0, '', iData, iCode);
end;

procedure TWebSocket.CallOnText(const iText: String);
begin
  CallEvent(TCallEventType.OpText, 0, iText, nil, TOpCode.Text);
end;

procedure TWebSocket.Close;
begin
  if FSocketThread = nil then
    Exit;

  FSocketThread.Close;
  FSocketThread := nil;
end;

procedure TWebSocket.Connect(const iURL: String);
begin
  if FSocketThread <> nil then
    Close;

  if FSocketThread = nil then
    FSocketThread := TSocketThread.Create(Self);

  FURL := iURL;

  FSocketThread.FState := TSocketThread.TState.TryConnect;
end;

constructor TWebSocket.Create(iOwner: TComponent);
begin
  inherited;

  Randomize;

  FUserAgent := DEF_USER_AGENT;
  FHandshakeTimeout := DEF_HANDSHAKE_TIMEOUT;
  FHeartBeatInterval := DEF_HEARTBEAT_INTERVAL;
  FAsyncEvent := True;
end;

class function TWebSocket.DecodeFrame(
  const iData: TBytes;
  out oFrameSize: Integer;
  out oFIN: Boolean;
  out oOpCode: TOpCode;
  out oResult: TBytes): Boolean;
begin
  Result :=
    TSocketThread.DecodeFrame(iData, oFrameSize, oFIN, oOpCode, oResult);
end;

destructor TWebSocket.Destroy;
begin
  Close;
  inherited;
end;

function TWebSocket.GetConnected: Boolean;
begin
  if (FSocketThread = nil) or (FSocketThread.FSocket = nil) then
    Result := False
  else
    Result := FSocketThread.FSocket.GetConnected;
end;

procedure TWebSocket.ResetHeartbeat;
begin
  FSocketThread.FHeartBeatPrev := Now;
end;

procedure TWebSocket.Send(const iData: TBytes);
begin
  FSocketThread.SendBinary(iData);
end;

procedure TWebSocket.SendPing;
begin
  FSocketThread.SendOpCode(TOpCode.Ping);
end;

procedure TWebSocket.Send(const iText: String);
begin
  FSocketThread.SendText(iText);
end;

end.

